Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'
  resources :stories do
#    resources :tags
#    resources :accounts
    resources :idea
    resources :scenes do
      resources :comment
    end
#    resources :roles
#    resources :requests
  end

#  resources :scene do
#    resources :comment
#  end

#  resources :account do
#    resources :story
#  end

  get '/register' => 'account#new'
  post '/register' => 'account#create'
  get '/user/:id' => 'account#show', as: "user_page"
  get '/user/edit' => 'account#edit'
  put '/user/edit' => 'account#update'
  get '/users' => 'account#index'
  post '/role' => 'story_user#create'
  delete '/role' => 'story_user#destroy'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'

  delete '/logout' => 'sessions#destroy'

#  get '/story/new' => 'stories#new'
#  post '/story/new' => 'stories#create'
#  get '/story/:id' => 'stories#show'
#  get '/story/:id/edit' => 'stories#edit'
#  put '/story/:id/edit' => 'stories#update'

#  get '/scene/:id' => 'scenes#show'
#  put '/scene/:id' => 'scenes#update'
#  get '/scene/:id/edit' => 'scenes#edit'
#  put '/scene/:id/edit' => 'scenes#update'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
