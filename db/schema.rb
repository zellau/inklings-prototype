# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160504011628) do

  create_table "accounts", force: :cascade do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "salt"
    t.time     "midnight"
    t.integer  "daily_goal"
    t.integer  "streak"
    t.integer  "points"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "email"
  end

  create_table "comments", force: :cascade do |t|
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
    t.integer  "scene_id"
    t.integer  "seen"
  end

  add_index "comments", ["account_id"], name: "index_comments_on_account_id"
  add_index "comments", ["scene_id"], name: "index_comments_on_scene_id"

  create_table "ideas", force: :cascade do |t|
    t.string   "title"
    t.text     "body"
    t.integer  "show"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
    t.integer  "story_id"
  end

  add_index "ideas", ["account_id"], name: "index_ideas_on_account_id"
  add_index "ideas", ["story_id"], name: "index_ideas_on_story_id"

  create_table "requests", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "scene_id"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "role"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "scenes", force: :cascade do |t|
    t.integer  "order"
    t.integer  "show"
    t.integer  "published"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "story_id"
  end

  add_index "scenes", ["story_id"], name: "index_scenes_on_story_id"

  create_table "stories", force: :cascade do |t|
    t.string   "title"
    t.string   "concept"
    t.integer  "publish_status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "story_searches", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "story_id"
    t.integer  "role_id"
  end

  add_index "story_searches", ["role_id"], name: "index_story_searches_on_role_id"
  add_index "story_searches", ["story_id"], name: "index_story_searches_on_story_id"

  create_table "story_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "story_id"
    t.integer  "tag_id"
  end

  add_index "story_tags", ["story_id"], name: "index_story_tags_on_story_id"
  add_index "story_tags", ["tag_id"], name: "index_story_tags_on_tag_id"

  create_table "story_users", force: :cascade do |t|
    t.integer  "approved"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "story_id"
    t.integer  "account_id"
    t.integer  "role_id"
  end

  add_index "story_users", ["account_id"], name: "index_story_users_on_account_id"
  add_index "story_users", ["role_id"], name: "index_story_users_on_role_id"
  add_index "story_users", ["story_id"], name: "index_story_users_on_story_id"

  create_table "tags", force: :cascade do |t|
    t.string   "tag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_searches", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
    t.integer  "role_id"
  end

  add_index "user_searches", ["account_id"], name: "index_user_searches_on_account_id"
  add_index "user_searches", ["role_id"], name: "index_user_searches_on_role_id"

  create_table "user_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
    t.integer  "tag_id"
  end

  add_index "user_tags", ["account_id"], name: "index_user_tags_on_account_id"
  add_index "user_tags", ["tag_id"], name: "index_user_tags_on_tag_id"

end
