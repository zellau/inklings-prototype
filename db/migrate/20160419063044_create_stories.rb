class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :title
      t.string :concept
      t.integer :publish_status

      t.timestamps null: false
    end
  end
end
