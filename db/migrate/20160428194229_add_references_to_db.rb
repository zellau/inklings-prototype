class AddReferencesToDb < ActiveRecord::Migration
  def change
    add_reference :ideas, :accounts, index: true, foreign_key: true
    add_reference :ideas, :stories, index: true, foreign_key: true
    add_reference :user_tags, :accounts, index: true, foreign_key: true
    add_reference :user_tags, :tags, index: true, foreign_key: true
    add_reference :user_searches, :accounts, index: true, foreign_key: true
    add_reference :user_searches, :roles, index: true, foreign_key: true
    add_reference :story_searches, :stories, index: true, foreign_key: true
    add_reference :story_searches, :roles, index: true, foreign_key: true
    add_reference :story_tags, :stories, index: true, foreign_key: true
    add_reference :story_tags, :tags, index: true, foreign_key: true
    add_reference :story_users, :stories, index: true, foreign_key: true
    add_reference :story_users, :accounts, index: true, foreign_key: true
    add_reference :story_users, :roles, index: true, foreign_key: true
    add_reference :scenes, :stories, index: true, foreign_key: true
  end
end
