class CreateStorySearches < ActiveRecord::Migration
  def change
    create_table :story_searches do |t|

      t.timestamps null: false
    end
  end
end
