class ChangeForeignKeys < ActiveRecord::Migration
  def change
    rename_column :ideas, :accounts_id, :account_id
    rename_column :ideas, :stories_id, :story_id
    rename_column :user_tags, :accounts_id, :account_id
    rename_column :user_tags, :tags_id, :tag_id
    rename_column :user_searches, :accounts_id, :account_id
    rename_column :user_searches, :roles_id, :role_id
    rename_column :story_searches, :stories_id, :story_id
    rename_column :story_searches, :roles_id, :role_id
    rename_column :story_tags, :stories_id, :story_id
    rename_column :story_tags, :tags_id, :tag_id
    rename_column :story_users, :stories_id, :story_id
    rename_column :story_users, :accounts_id, :account_id
    rename_column :story_users, :roles_id, :role_id
    rename_column :scenes, :stories_id, :story_id
  end
end
