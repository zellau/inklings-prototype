class CreateStoryTags < ActiveRecord::Migration
  def change
    create_table :story_tags do |t|

      t.timestamps null: false
    end
  end
end
