class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :username
      t.string :hashword
      t.string :salt
      t.time :midnight
      t.integer :daily_goal
      t.integer :streak
      t.integer :points

      t.timestamps null: false
    end
  end
end
