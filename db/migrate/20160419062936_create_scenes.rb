class CreateScenes < ActiveRecord::Migration
  def change
    create_table :scenes do |t|
      t.integer :order
      t.integer :show
      t.integer :published
      t.text :body

      t.timestamps null: false
    end
  end
end
