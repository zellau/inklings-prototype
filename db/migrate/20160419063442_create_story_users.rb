class CreateStoryUsers < ActiveRecord::Migration
  def change
    create_table :story_users do |t|
      t.integer :approved

      t.timestamps null: false
    end
  end
end
