class FixComments < ActiveRecord::Migration
  def change
    rename_column :comments, :scenes_id, :scene_id
    rename_column :comments, :accounts_id, :account_id

  end
end
