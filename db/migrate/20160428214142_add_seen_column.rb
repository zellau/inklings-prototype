class AddSeenColumn < ActiveRecord::Migration
  def change
    add_column :comments, :seen, :integer
  end
end
