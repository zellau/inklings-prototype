class CreateUserSearches < ActiveRecord::Migration
  def change
    create_table :user_searches do |t|

      t.timestamps null: false
    end
  end
end
