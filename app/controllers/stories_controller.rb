class StoriesController < ApplicationController
  def index
    @stories = Story.all

    get_bookshelf
  end

  def show
     @story = Story.find(params[:id])

    get_bookshelf
  end

  def new
    @story = Story.new
    @people = @story.story_users.where(role_id: Role.where(description: "Writer").ids)
    @writers = @story.accounts.where(id: @people)

    get_bookshelf
  end

  def edit
    @story = Story.find(params[:id])
    @people = @story.story_users.where(role_id: Role.where(description: "Writer").ids)
    @writers = @story.accounts.where(id: @people)

    get_bookshelf
  end
  
  def create
    @account = Account.find(session[:user_id])
    @story = @account.stories.create(story_params)
#    @story_user = @account.story_users.create(story_user_params)

#    @story.text = @text

    @story_user = StoryUser.find_by(story_id: @story, account_id: @account)
    @story_user.role_id = Role.where(description: "Writer").first.id
    @story_user.approved = 1
    @story_user.save

    redirect_to @story
  end

  def update
    @story = Story.find(params[:id])

    if @story.update(story_params)
      redirect_to '/story/' + params[:id]
#      redirect_to "show", id: @story.id
    else
      redirect_to action: "edit", id: @story.id
    end
  end

  def destroy
  end

  def get_bookshelf
    if current_user
      @current = Account.find(session[:user_id])
      @bookshelf_ids = @current.story_users.where(role_id: Role.where(description: "Writer").ids)
      @notebook = @current.stories.where.not(id: @bookshelf_ids)
    end
  end

  private
  def story_params
    params.require(:story).permit(:title, :concept, :publish_status)
  end

  def story_user_params
    params.require(:story_user).permit(:story, :user, :role_id)
  end

end
