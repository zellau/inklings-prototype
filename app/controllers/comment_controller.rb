class CommentController < ApplicationController
  def create
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:scene_id])
    @comment = @scene.comments.create(comment_params)
    @comment.account_id = session[:user_id]
    @comment.save
    redirect_to story_scene_path(@story, @scene)
  end

  def destroy
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:scene_id])
    @comment = @scene.comments.find(params[:id])
    @comment.destroy
    redirect_to story_scene_path(@story, @scene)
  end

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end
