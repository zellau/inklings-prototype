class ScenesController < ApplicationController
  def show
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:id])
  end

  def edit
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:id])
  end

  def create
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.create()

#    if @scene.save
      redirect_to action: "edit", id: @scene.id
#    else
#      redirect_to @story
#    end
  end

  def update
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:id])
    
    @scene.update(scene_params)
      redirect_to @story
#    else
#      render 'edit'
#    end
  end
  
  def destroy
    @story = Story.find(params[:story_id])
    @scene = @story.scenes.find(params[:id])
    @scene.destroy

    redirect_to @story
  end

  private
    def scene_params
      params.require(:scene).permit(:body, :order, :show, :published)
    end
end
