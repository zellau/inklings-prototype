class StoryUserController < ApplicationController
  def create
    @role = StoryUser.new(role_params)
    @role.save
    redirect_to "/stories"
  end

  def destroy
  end

  private
    def role_params
      params.require(:user_story).permit(:story_id, :account_id, :role_id)
    end
end
