class AccountController < ApplicationController
  def index
    @accounts = Account.all

    if current_user
      @current = Account.find(session[:user_id])
#      @bookshelf_ids = @current.story_users.where(role_id: Role.where(description: "Writer").ids)
      @notebook = @current.stories.where.not(id: @bookshelf_ids)
#    @bookshelf_ids = @current.story_users.where(role_id: Role.where(description: "Editor").ids[0])
#    @notebook_ids = @current.story_users.where(role_id: Role.where(description: "Writer").ids[0])
#    @bookshelf = Array.new(0)
#    @notebook = Array.new(0)
#    for id in @bookshelf_ids
#      @bookshelf << @current.stories.where(id: id.story_id)
#    end
#    for id in @notebook_ids
#      for story in @current.stories.where(id: id.story_id)
#        @notebook << story
#      end
#    end

    end
  end

  def show
    @account = Account.find(params[:id])
    @bookshelf_ids = @account.story_users.where(role_id: Role.where(description: "Editor").ids[0])
    @notebook_ids = @account.story_users.where(role_id: Role.where(description: "Writer").ids[0])
    @bookshelf = Array.new(0)
    @notebook = Array.new(0)
    for id in @bookshelf_ids
      @bookshelf << @account.stories.where(id: id.story_id)
    end
    for id in @notebook_ids
      @notebook << @account.stories.where(id: id.story_id)
    end
#    @notebook = @account.stories-@bookshelf.first
#    @bookshelf = @account.stories.where(id: @bookshelf_ids)
#    @notebook = @account.stories.where.not(id: @bookshelf_ids)
  end

  def new
    @account = Account.new
  end

  def edit
    @account = Account.find(params[:id])
  end

  def create
    @account = Account.new(account_params)

    if @account.save
      session[:user_id] = @account.id
      redirect_to '/'
    else
      render 'new'
    end
  end

  def update
    @account = Account.find(params[:id])

    if @account.update(account_params)
      redirect_to '/'
    else
      render 'edit'
    end
  end

  def destroy
    @account = Account.find(params[:id])
    @account.destroy

    redirect_to 'new'
  end

  private
    def account_params
      params.require(:account).permit(:username, :email, :password, :midnight, :daily_goal, :streak, :points)
    end

end
