class StoryController < ApplicationController
  def show
     @story = Story.find(params[:id])
  end

  def new
    @story = Story.new
#    @story_user = StoryUser.new
  end

  def edit
    
  end
  
  def create
    @account = Account.find(session[:user_id])
    @story = @account.stories.create(story_params)
#    @story_user = @account.story_users.create(story_user_params)

#    @story.text = @text

    @story_user = StoryUser.find_by(story_id: @story, account_id: @account)
    @story_user.role = Role.find_by(description: "Writer")
    @story_user.approved = 1

    redirect_to action: "show", id: @story.id
  end

  def update
  end

  def destroy
  end

  private
  def story_params
    params.require(:story).permit(:title, :concept, :publish_status)
  end

  def story_user_params
    params.require(:story_user).permit(:story, :user, :role)
  end

end
