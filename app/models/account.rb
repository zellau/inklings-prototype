class Account < ActiveRecord::Base
  has_many :ideas

  has_many :user_searches
  has_many :roles, through: :user_searches

  has_many :user_tags
  has_many :tags, through: :user_tags

  has_many :story_users
  has_many :stories, through: :story_users
  has_many :roles, through: :story_users

  has_many :comments

  def story_role?(story)
    story_user = story_users.where(:story => story)
    story_user.role
  end

  has_secure_password

end
