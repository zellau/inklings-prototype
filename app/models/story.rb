class Story < ActiveRecord::Base
  has_many :scenes
  has_many :ideas

  has_many :story_users
  has_many :accounts, through: :story_users
  has_many :user_roles, through: :story_users, class_name: 'Role'

  has_many :story_tags
  has_many :tags, through: :story_tags

  has_many :story_searches
  has_many :roles, through: :story_searches

  def user_role?(user)
    story_user = story_users.where(:user => user)
    story_user.role
  end

end
