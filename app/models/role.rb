class Role < ActiveRecord::Base
  has_many :user_searches
  has_many :story_searches
  has_many :story_users
end
