class StoryUser < ActiveRecord::Base
  belongs_to :story
  belongs_to :account
  belongs_to :role
end
